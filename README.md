# LendLedger CLI Based Application
This application describes the contract between the LL platform and particular Loan Management Systems which someone wants to integrate into the LL platform. This integration is named as Blockchain Integration Layer using Command Line.

This document may concern users which want to access LL platform using command line.

## API for developers
The lendledger CLI contains information to transact using CLI

## Deployment
The project is hosted on GitHub. 

### Prerequisites
Make sure you have Node.js 8.0.0 or higher installed. If not, install it

```sh
# Check your node version using this command
node --version
```
```sh
# Access the CLI application using the below command 
npm install -g lendledger-cli
```

## How to use lendledger-cli
```sh
<network-type> : test/public 

<account-id> : Provided during create account.

<account-seed> : Provided during create account.

# Create Account 
lendledger create -n <network-type> 

# Create Account from Mnemonic(array of strings) which uses BIP39 protocol.
lendledger mnemonic -n <network-type> -m <mnemonic> -a <accounts>
Example :- lendledger mnemonic -n test -m "This is an example of mnemonic array of strings" -a 1

# Login to perform any transaction. Session is valid till 30 minutes. 
lendledger login -n <network-type> -a <account-id>

# Logout from current session 
lendledger logout

# Check the user currently logged into the system 
lendledger whoami

# Get Balance for an Account  
lendledger balance -n <network-type> -a <account-id>

# Audit Trail for an Account 
lendledger audit -n <network-type> -a <account-id>

# Change Trusline for an account to accept asset as currency 
lendledger changeTrust -n <network-type> -f <first-name> -l <last-name> -s <account-seed> -a <asset-name> -c <account-no> -i <account-ifsc>

# Issuer issues asset after user has changed trustline to accept asset as currency. 
lendledger issueAsset -n <network-type> -s <account-seed> -a <asset-name> -r <receiver-account-id> -m <asset-mount>

# Account Holder creates or manages offer inorder to buy or sell asset. 
lendledger manageOffer -n <network-type> -s <account-seed> -d <selling-asset-name> -i <selling-asset-issuer-account-id> -b <buying-asset-name> -j <buying-asset-issuer-account-id> -a <amount-to-sell> -p <asset-price-per-unit> -o <offer-id>

# Get all offer created by an account-id 
lendledger getOffer -n <network-type> -a <account-id>

# Lender lends owned asset to any account holder (Borrower) 
lendledger lend -n <network-type> -s <account-seed> -l <amount-to-lend> -m <memo-remarks> -a <assset-name> -b <borrower-account-id>

# Account holder (Borrower) exchanges asset to any fiat(USD/EUR/INR)
lendledger exchange -n <network-type> -s <account-seed> -e <amount-to-exchcange> -m <memo-remarks> -a <assset-name>


```
