/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 09-August-2018
 */
const minimist = require('minimist')
const error = require('./utils/error')
const fs = require('fs');
const path = require('path');

module.exports = () => {
    const args = minimist(process.argv.slice(2))

    let cmd = args._[0] || 'help'

    if (typeof args._[0] === "undefined") {
        cmd = '';
    }

    if (args.version || args.v) {
        cmd = 'version'
    }

    if (args.help || args.h) {
        cmd = 'help'
    }

    switch (cmd) {
        case 'login':
            require('./cmds/auth').login(args)
            break

        case 'logout':
            require('./cmds/auth').logout(args)
            break

        case 'whoami':
            require('./cmds/auth').whoami(args)
            break

        case 'create':
            require('./cmds/account').create(args)
            break

        case 'mnemonic':
            require('./cmds/account').mnemonic(args)
            break

        case 'balance':
            require('./cmds/account').getAccount(args)
            break

        case 'audit':
            require('./cmds/account').getAccountAudit(args)
            break

        case 'changeTrust':
            require('./cmds/asset').changeTrust(args)
            break

        case 'issueAsset':
            require('./cmds/asset').issueAsset(args)
            break

        case 'manageOffer':
            require('./cmds/offer').manageOffer(args)
            break

        case 'getOffer':
            require('./cmds/offer').getOffer(args)
            break

        case 'lend':
            require('./cmds/payment').lend(args)
            break

        case 'exchange':
            require('./cmds/payment').exchange(args)
            break

        case 'help':
            require('./cmds/help')(args)
            break

        default:
            printMotd();
            console.log('');
            error(`                Run 'lendledger --help' for more information.`, true)
            break
    }
}

/**
 * Private Functions
 */
function printMotd() {
    var dt = fs.readFileSync(path.join(__dirname, './utils/print'));
    console.log(dt.toString());
}