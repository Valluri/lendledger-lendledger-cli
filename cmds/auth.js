/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 23-August-2018
 */

'use strict';


Object.defineProperty(exports, "__esModule", {
    value: true
});
var config = require('../config/config.json');
const ora = require('ora')
var StellarSdk = require('stellar-sdk');
var persist = require('../persist/persist-store')
// const crypto = require('crypto2');
// const cipher = crypto.createCipher('aes192', 'secret_to_decipher');
// const decipher = crypto.createDecipher('aes192', 'secret_to_decipher');


exports.login = exports.logout = exports.whoami = undefined;

exports.login = login;
exports.logout = logout;
exports.whoami = whoami;

/**
 * @author:Girijashankar Mishra
 * @description: This function is used to authenticate user to perform any transaction.
 * @param {networkType, accountId} args 
 * @param {JSON} res
 */
async function login(args) {
    const spinner = ora().start();
    persist.store.clear();
    try {
        // const password = await crypto.createPassword('secret_to_decipher');
        // const iv = await crypto.createIv();

        const network = args.networkType || args.n;
        const accountId = args.accountId || args.a;

        // const encrypted = await crypto.encrypt(accountId, password, iv);

        var server;
        if (network === "test") {
            server = new StellarSdk.Server(config.stellarServerTest);
            StellarSdk.Network.useTestNetwork();
        } else if (network === "public") {
            server = new StellarSdk.Server(config.stellarServerPublic);
            StellarSdk.Network.usePublicNetwork();
        }

        server.loadAccount(accountId) // If the account is not found, surface a nicer error message for logging.
            .catch(StellarSdk.NotFoundError, function (error) {
                //   throw new Error('The destination account does not exist!');
                console.log('Provided account does not exist.')
            }) // If there was no error, load up-to-date information on your account.
            .then(function (data) {

                // console.log(encrypted);
                persist.store.setItem('accountId', accountId, {
                    ttl: 30000 * 60 /* 30 min */
                });
                console.log(accountId, ' has successfully logged into the system.')
            });
        spinner.stop();
    } catch (err) {
        spinner.stop();
        console.error(err);
    }
}

/**
 * @author:Girijashankar Mishra
 * @description: This function is used to logout user.
 * @param {} args 
 * @param {JSON} res
 */
async function logout(args) {
    const spinner = ora().start();
    try {
        persist.store.clear();
        console.log("You have been logged out from the system.");
        spinner.stop();
    } catch (err) {
        spinner.stop();
        console.error(err);
    }
}

/**
 * @author:Girijashankar Mishra
 * @description: This function will give the current login details if exists.
 * @param {} args 
 * @param {JSON} res
 */
async function whoami(args) {
    const spinner = ora().start();
    try {
        persist.store.getItem('accountId').then(function (data) {
            if (data) {
                // decrypt(data, function (decrypted) {
                //     console.log(decrypted);
                // });
                console.log(data)

                // return data;
            } else {
                console.log('Please Login.')
            }
        });                                                                                                                                                                                     
        spinner.stop();
    } catch (err) {
        spinner.stop();
        console.error(err);
    }
}

// async function decrypt(accountId, callback) {
//     const password = await crypto.createPassword('secret_to_decipher');
//     const iv = await crypto.createIv();
//     const decrypted = await crypto.decrypt(accountId, password, iv);
//     callback(err,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               decrypted);
// }