/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 09-August-2018
 */
const menus = {
  main: `
      lendledger [command] <options>
  
      auth      ............    login, logout, whoami
      account   ............    create, mnemonic, balance, audit 
      asset     ............    change trustline, issueAsset
      offer     ............    manageOffer, getOffer
      payment   ............    lend, exchange
      help      ............    show help menu for a command`,

  auth: `
    lendledger <commands>
      commands:-
        login, -n networkType, -a accountId ..... Login to Lendedger network to perform any transaction. Session is valid till 30 minutes.
        logout                              ..... Logout from Lendedger network
        whoami                              ..... Check current logged in user to stellar network`,

  account: `
    lendledger <commands>
      commands:-
        create, -n networkType                               ..... Creates account in stellar network
        mnemonic, -n networkType, -m mnemonic, -a accounts   ..... Creates account in stellar network using array of strings(mnemonic seed)
        balance,  -n networkType, -a accountId               ..... Balance for an account
        audit,  -n networkType, -a accountId                 ..... Audit for an accountId`,

  asset: `
    lendledger <commands>
      commands:-
        changeTrust, -n networkType, -f firstName, -l lastName, -s accountSeed, -a asset, -c accountNo, -i accountIfsc  ..... Account holder willing to accept asset
        issueAsset, -n networkType, -s issuerSeed, -a asset, -r receiver, -m assetAmount                                ..... Asset issuer issues asset`,

  offer: `
    lendledger <commands>
      commands:-
        manageOffer, -n networkType, -s accountSeed, -d sellingAsset, -i sellingAssetIssuer, -b buyingAsset, -g buyingAssetIssuer, -a amountToSell, -p assetPricePerUnit, -o  offerId  ..... Account holder creates an offer
        getOffer, -n networkType, -a accountId                                                                                                                                         ..... Account holder checks all offer created from an account`,

  payment: `
    lendledger <commands>
      commands:-
        lend, -n networkType, -s accountSeed, -b borrowerId, -m amount, -m memo, -a assetName ..... Lender lends asset to Borrower
        exchange, -n networkType, -s accountSeed, -m amount, -m memo, -a assetName            ..... Borrower exchanges asset to fiat from CreditNode`,
}

module.exports = (args) => {
  const subCmd = args._[0] === 'help' ?
    args._[1] :
    args._[0]

  console.log(menus[subCmd] || menus.main)
}