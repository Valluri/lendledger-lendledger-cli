/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 09-August-2018
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var config = require('../config/config.json');
const ora = require('ora')
var persist = require('../persist/persist-store')

// var auth = require('./auth');
var StellarSdk = require('stellar-sdk');
var request = require('request');
var hdWallet = require('stellar-hd-wallet');
var bip39 = require('bip39');
var readlineSync = require('readline-sync');
var auth = require('./auth');

exports.create = exports.getAccount = exports.getAccountAudit = exports.mnemonic = undefined;

exports.create = create;
exports.getAccount = getAccountDetails;
exports.getAccountAudit = getAccountAudit;
exports.mnemonic = mnemonic;

/**
 * @author Girijashankar Mishra
 * @description Create account on Stellar Network.
 * @param {networkType} args 
 * @param {JSONObject} json 
 */
async function create(args) {
    // var deferred = Q.defer();
    const spinner = ora().start();
    try {
        var pair = StellarSdk.Keypair.random();
        const network = args.networkType || args.n;

        var account = {};
        var accountId = pair.publicKey();
        var accountSeed = pair.secret();
        account["accountId"] = accountId;
        account["accountSeed"] = accountSeed;

        if (network === "test") {
            request.get({
                url: config.serverUrl,
                qs: {
                    addr: accountId
                },
                json: true
            }, function (error, response, body) {
                if (error || response.statusCode !== 200) {
                    // console.error('ERROR!', error || body);
                } else {
                    account["remarks"] = "Account has been funded using test network.";
                }
            });
        } else if (network === "public") {
            account["remarks"] = "Please fund your account to activate all services.";
        }
        spinner.stop();
        console.log(JSON.parse(JSON.stringify(account)));
        console.log("Account Created Successfully. Please store your accountId and accountSeed safely");
    } catch (err) {
        spinner.stop();
        console.error(err)
    }
}

/**
 * @author Girijashankar Mishra
 * @description Get Account details from Stellar Network based on accountId
 * @param {networkType,accountId} args 
 * @param {AccountResponse}  
 */
async function getAccountDetails(args) {
    const spinner = ora().start()

    try {
        var accountId = args.accountId || args.a;
        const network = args.networkType || args.n;
        persist.store.getItem('accountId').then(function (data) {
            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
            }
            if (data) {
                if (data === accountId) {
                    // the JS SDK uses promises for most actions, such as retrieving an account
                    balance(server, accountId);
                    spinner.stop();

                } else {
                    console.log('Please login to complete your transaction.')
                    var userAccountId = readlineSync.question('Enter your AccountId : ');
                    var arg = {};
                    arg["n"] = "test";
                    arg["a"] = userAccountId;
                    var resp = auth.login(arg);
                    // the JS SDK uses promises for most actions, such as retrieving an account
                    resp = balance(server, userAccountId);
                    spinner.stop();

                }
            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                // the JS SDK uses promises for most actions, such as retrieving an account
                resp = balance(server, userAccountId);
                spinner.stop();

            }
            // spinner.stop();
        });
    } catch (err) {
        spinner.stop();
        console.error(err);
    }

}

/**
 * @author Girijashankar Mishra
 * @description Get all activities performed for an account from Stellar Network based on accountId
 * @param {networkType,accountId} args 
 * @param {CollectionPage<EffectRecord>}  
 */
async function getAccountAudit(args) {
    const spinner = ora().start()

    try {
        var accountId = args.accountId || args.a;
        const network = args.networkType || args.n;
        // let whoami = await auth.whoami();
        // if (whoami === accountId) {
        persist.store.getItem('accountId').then(function (data) {
            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
            }
            if (data) {
                if (data === accountId) {

                    audit(server, accountId);
                    spinner.stop();

                } else {
                    console.log('Please login to complete your transaction.');
                    var userAccountId = readlineSync.question('Enter your AccountId : ');
                    var arg = {};
                    arg["n"] = "test";
                    arg["a"] = userAccountId;
                    var resp = auth.login(arg);

                    resp = audit(server, userAccountId);
                    spinner.stop();

                }
            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);

                resp = audit(server, userAccountId);
                spinner.stop();

            }
        });
    } catch (err) {
        spinner.stop()

        console.error(err)
    }
}



/**
 * @author Girijashankar Mishra
 * @description Create account on Stellar Network using mnemonic
 * @param {networkType} args 
 * @param {JSONObject} json 
 */
async function mnemonic(args) {
    // var deferred = Q.defer();
    const spinner = ora().start();
    try {
        const network = args.networkType || args.n;
        const mnemonic = args.mnemonic || args.m;
        const noOfAccounts = args.accounts || args.a;
        // console.log("mnemonic val ===> ", mnemonic)
        // If mnemonic hash does not exist in DB then create an account using mnemonic string.
        //Convert mnemonic string to SeedHex String using BIP39 protocol.
        var bip39hexSeed = bip39.mnemonicToSeedHex(mnemonic);

        //Generate HD-Wallet from SeedHex String obtained in previous step. 
        //Inside wallet you will get PublicKey and Secret 
        const wallet = hdWallet.fromSeed(bip39hexSeed);
        var accounts = [];
        if (noOfAccounts > 0) {
            for (var i = 0; i < noOfAccounts; i++) {
                var accountNo = {};
                var account = {};
                var accountId = wallet.getPublicKey(i);
                var accountSeed = wallet.getSecret(i);
                account["accountId"] = accountId;
                account["accountSeed"] = accountSeed;
                wallet.derive(`m/44'/148'/0'`);

                if (network === "test") {
                    request.get({
                        url: config.serverUrl,
                        qs: {
                            addr: wallet.getPublicKey(i)
                        },
                        json: true
                    }, function (error, response, body) {
                        if (error || response.statusCode !== 200) {
                            // console.error('ERROR!', error || body);
                        }
                    });
                    account["remarks"] = "Account has been funded using test network.";

                } else if (network === "public") {
                    account["remarks"] = "Please fund your account to activate all services.";
                }
                accountNo[i] = account;
                accounts.push(accountNo);
            }

            spinner.stop();
            console.log(JSON.parse(JSON.stringify(accounts)));
            console.log("Account Created Successfully. Please store your mnemonic, accountId and accountSeed safely");
            console.log("In case you forgot your accountId and accountSeed you can obtain it by using your mnemonic seed.");
        } else {
            console.error("Accounts to be created using mnemonic seed should be greater than 0.");
            spinner.stop();
        }
    } catch (err) {
        spinner.stop();
        console.error(err)
    }
}


/**
 * @author Girijashankar Mishra
 * @description Get all activities performed for an account from Stellar Network based on accountId
 * @param {server,accountId} args 
 * @param {CollectionPage<EffectRecord>}  
 */
function audit(server, accountId) {
    server.effects()
        .forAccount(accountId)
        .call()
        .then(function (effectResults) {
            //page 1
            // console.log(effectResults.records)
            console.log(JSON.parse(JSON.stringify(effectResults)));
            return effectResults;
        })
        .catch(function (err) {
            // console.log(err);
            console.error(err)
        });
}

/**
 * @author Girijashankar Mishra
 * @description Get Account details from Stellar Network based on accountId
 * @param {server,accountId} args 
 * @param {AccountResponse}  
 */
function balance(server, accountId) {
    server.loadAccount(accountId).then(function (account) {
        console.log(JSON.parse(JSON.stringify(account)));
        return account;
    });
}