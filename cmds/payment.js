/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 10-August-2018
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var config = require('../config/config.json');
const ora = require('ora')
var readlineSync = require('readline-sync');
var auth = require('./auth');

var StellarSdk = require('stellar-sdk');
var request = require('request');
var persist = require('../persist/persist-store')

exports.lend = exports.exchange = undefined;

exports.lend = lend;
exports.exchange = exchange;

/**
 * @author Girijashankar Mishra
 * @description Lender lends money in the form of asset to borrower.
 * @param {networkType,accountSeed,borrowerId,amount,memo,assetName} args 
 * @param {*} res 
 */
async function lend(args) {
    const spinner = ora().start();
    try {
        persist.store.getItem('accountId').then(function (data) {
            const network = args.networkType || args.n;
            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
                StellarSdk.Network.useTestNetwork();
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
                StellarSdk.Network.usePublicNetwork();
            }
            if (data) {
                lendAsset(server, args, spinner);
                spinner.stop();

            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                lendAsset(server, args, spinner);
                spinner.stop();
            }
            spinner.stop();
        });
    } catch (err) {
        spinner.stop();
        console.error(err)
    }
}

/**
 * @author Girijashankar Mishra
 * @description Borrower exchanges asset to fiat(currency) from credit node.
 * @param {networkType,accountSeed,amount,memo,assetName} args 
 * @param {*} res 
 */
async function exchange(args) {
    const spinner = ora().start();
    try {
        persist.store.getItem('accountId').then(function (data) {
            const network = args.networkType || args.n;
            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
                StellarSdk.Network.useTestNetwork();
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
                StellarSdk.Network.usePublicNetwork();
            }
            if (data) {
                exchangeAsset(server, args, spinner);
                spinner.stop();

            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                exchangeAsset(server, args, spinner);
                spinner.stop();
            }
            spinner.stop();
        });

    } catch (err) {
        spinner.stop();
        console.error(err);
    }
}

/**
 * @author Girijashankar Mishra
 * @description Lender lends money in the form of asset to borrower.
 * @param {server, args, spinner} args 
 * @param {*} res 
 */
function lendAsset(server, args, spinner) {
    const seed = args.accountSeed || args.s;
    const borrowerId = args.borrowerId || args.b;
    const amount = args.amount || args.l;
    const memo = args.memo || args.m;
    const assetName = args.assetName || args.a;


    var transaction;
    var sourceKeys = StellarSdk.Keypair.fromSecret(seed);
    server.loadAccount(borrowerId) // If the account is not found, surface a nicer error message for logging.
        .catch(StellarSdk.NotFoundError, function (error) {
            //   throw new Error('The destination account does not exist!');
            console.log('Provided borrower account does not exist!')
        }) // If there was no error, load up-to-date information on your account.
        .then(function () {
            return server.loadAccount(sourceKeys.publicKey());
        }).then(function (sourceAccount) {
            var balances = JSON.parse(JSON.stringify(sourceAccount)).balances;
            // console.log('SourceAccount balances === ' + JSON.stringify(balances));
            var issuerId = "";

            for (var i = 0; i < balances.length; i++) {
                var asset_code = balances[i].asset_code;
                if (assetName === asset_code) {
                    issuerId = balances[i].asset_issuer;
                }
            }
            // Start building the transaction.
            transaction = new StellarSdk.TransactionBuilder(sourceAccount)
                .addOperation(StellarSdk.Operation.payment({
                    destination: borrowerId,
                    // Because Stellar allows transaction in many currencies, you must
                    // specify the asset type. The special "native" asset represents Lumens.
                    // asset: StellarSdk.Asset.native(),
                    asset: new StellarSdk.Asset(assetName, issuerId),
                    amount: amount
                }))
                // A memo allows you to add your own metadata to a transaction. It's
                // optional and does not affect how Stellar treats the transaction.
                .addMemo(StellarSdk.Memo.text(memo))
                .build();
            // Sign the transaction to prove you are actually the person sending it.
            transaction.sign(sourceKeys);
            // And finally, send it off to Stellar!
            return server.submitTransaction(transaction);
        }).then(function (result) {
            spinner.stop();
            console.log(JSON.parse(JSON.stringify(result)));
        })
        .catch(function (error) {
            spinner.stop();
            console.error('Lend Asset!', error);
        });
}

/**
 * @author Girijashankar Mishra
 * @description Borrower exchanges asset to fiat(currency) from credit node.
 * @param {server, args, spinner} args 
 * @param {*} res 
 */
function exchangeAsset(server, args, spinner) {
    const creditNodeId = config.issuerAccount;
    const seed = args.accountSeed || args.s;
    const amount = args.amount || args.e;
    const memo = args.memo || args.m;
    const assetName = args.assetName || args.a;

    var transaction;
    var sourceKeys = StellarSdk.Keypair.fromSecret(seed);

    // the transaction fee when the transaction fails.
    server.loadAccount(creditNodeId) // If the account is not found, surface a nicer error message for logging.
        .catch(StellarSdk.NotFoundError, function (error) {
            //   throw new Error('The destination account does not exist!');
            console.log('Provided CreditNode account does not exist!')
        }) // If there was no error, load up-to-date information on your account.
        .then(function () {
            return server.loadAccount(sourceKeys.publicKey());
        }).then(function (sourceAccount) {
            var balances = JSON.parse(JSON.stringify(sourceAccount)).balances;
            // console.log('SourceAccount balances === ' + JSON.stringify(balances));
            var issuerId = "";

            for (var i = 0; i < balances.length; i++) {
                var asset_code = balances[i].asset_code;
                if (assetName === asset_code) {
                    issuerId = balances[i].asset_issuer;
                }
            }
            // Start building the transaction.
            transaction = new StellarSdk.TransactionBuilder(sourceAccount)
                .addOperation(StellarSdk.Operation.payment({
                    destination: creditNodeId,
                    // Because Stellar allows transaction in many currencies, you must
                    // specify the asset type. The special "native" asset represents Lumens.
                    // asset: StellarSdk.Asset.native(),
                    asset: new StellarSdk.Asset(assetName, issuerId),
                    amount: amount
                }))
                // A memo allows you to add your own metadata to a transaction. It's
                // optional and does not affect how Stellar treats the transaction.
                .addMemo(StellarSdk.Memo.text(memo))
                .build();
            // Sign the transaction to prove you are actually the person sending it.
            transaction.sign(sourceKeys);
            // And finally, send it off to Stellar!
            return server.submitTransaction(transaction);
        }).then(function (result) {
            spinner.stop();
            console.log(JSON.parse(JSON.stringify(result)));
        })
        .catch(function (error) {
            spinner.stop();
            console.error('Exchange Asset!', error);
        });
}