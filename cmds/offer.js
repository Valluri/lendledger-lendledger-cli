/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 10-August-2018
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var config = require('../config/config.json');
const ora = require('ora')
var readlineSync = require('readline-sync');
var auth = require('./auth');

var StellarSdk = require('stellar-sdk');
var request = require('request');
var persist = require('../persist/persist-store')

exports.manageOffer = exports.getOffer = undefined;

exports.manageOffer = manageOffer;
exports.getOffer = getOffer;

/**
 * @author Girijashankar Mishra
 * @description Any account holder in stellar can create an offer using this for native or any asset 
 *              which they have holded.
 * @param {networkType,accountSeed,sellingAsset,sellingAssetIssuer,buyingAsset,buyingAssetIssuer,amountToSell,assetPricePerUnit,offerId} args 
 * @param {*} res 
 */
async function manageOffer(args) {
    const spinner = ora().start();
    try {
        persist.store.getItem('accountId').then(function (data) {
            const network = args.networkType || args.n;
            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
                StellarSdk.Network.useTestNetwork();
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
                StellarSdk.Network.usePublicNetwork();
            }
            if (data) {
                manage(server, args, spinner);
                spinner.stop();

            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                manage(server, args, spinner);
                spinner.stop();
            }
            spinner.stop();
        });
    } catch (err) {
        spinner.stop();
        console.error(err)
    }
}

/**
 * @author Girijashankar Mishra
 * @description Get Offers created by an account.
 * @param {networkType,accountId} args 
 * @param {_embedded.records} res 
 */
async function getOffer(async) {
    const spinner = ora().start()
    try {
        persist.store.getItem('accountId').then(function (data) {
            const network = args.networkType || args.n;
            const accountId = args.accountId || args.a;

            var url;
            if (network === "test") {
                url = config.stellarServerTest + '/accounts/' + accountId + '/offers';
            } else if (network === "public") {
                server = config.stellarServerPublic + '/accounts/' + accountId + '/offers';
            }
            if (data) {
                get(url, spinner);

            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                get(url, spinner);
                spinner.stop();
            }
            spinner.stop();
        });
    } catch (err) {
        spinner.stop();
        console.error(err);
    }
}


/**
 * @author Girijashankar Mishra
 * @description Any account holder in stellar can create an offer using this for native or any asset 
 *              which they have holded.
 * @param {server, args, spinner} args 
 * @param {*} res 
 */
function manage(server, args, spinner) {
    const seed = args.accountSeed || args.s;
    const sellingAsset = args.sellingAsset || args.sa;
    const sellingAssetIssuer = args.sellingAssetIssuer || args.si;
    const buyingAsset = args.buyingAsset || args.ba;
    const buyingAssetIssuer = args.buyingAssetIssuer || args.bi;
    const amountToSell = args.amountToSell || args.a;
    const assetPricePerUnit = args.assetPricePerUnit || args.p;
    const offerId = args.offerId || args.o;

    var transaction;
    var keyPairs = StellarSdk.Keypair.fromSecret(seed);

    // the transaction fee when the transaction fails.
    server.loadAccount(keyPairs.publicKey()) // If the account is not found, surface a nicer error message for logging.
        .catch(StellarSdk.NotFoundError, function (error) {
            //   throw new Error('The destination account does not exist!');
            console.log('The account does not exist!')
        }) // If there was no error, load up-to-date information on your account.
        .then(function (sourceAccount) {
            var assetSelling;
            if (sellingAsset === "native") {
                assetSelling = StellarSdk.Asset.native();
            } else {
                assetSelling = new StellarSdk.Asset(sellingAsset, sellingAssetIssuer);
            }
            var assetBuying;
            if (buyingAsset === "native") {
                assetBuying = StellarSdk.Asset.native();
            } else {
                assetBuying = new StellarSdk.Asset(buyingAsset, buyingAssetIssuer);
            }
            // Start building the transaction.
            transaction = new StellarSdk.TransactionBuilder(sourceAccount)
                .addOperation(StellarSdk.Operation.manageOffer({
                    selling: assetSelling,
                    buying: buyingAsset,
                    amount: amountToSell,
                    price: assetPricePerUnit,
                    offerId: offerId
                }))
                // A memo allows you to add your own metadata to a transaction. It's
                // optional and does not affect how Stellar treats the transaction.
                .addMemo(StellarSdk.Memo.text('Offer has been created'))
                .build();
            // Sign the transaction to prove you are actually the person sending it.
            transaction.sign(keyPairs);
            // And finally, send it off to Stellar!
            server.submitTransaction(transaction);
        }).then(function (result) {
            spinner.stop();
            console.log(JSON.parse(JSON.stringify(result)))
        })
        .catch(function (error) {
            spinner.stop();
            console.error('Offer Asset!', error);
        });
}

/**
 * @author Girijashankar Mishra
 * @description Get Offers created by an account.
 * @param {url, spinner} args 
 * @param {_embedded.records} res 
 */
function get(url, spinner) {
    request.get({
        url: url,
        json: true
    }, function (error, response, body) {
        if (error || response.statusCode !== 200) {
            spinner.stop();
            console.error('ERROR!', error || body);
        } else {
            spinner.stop();
            console.log(body._embedded.records)
        }
    });
}