/**
 * @author Girijashankar Mishra
 * @version 1.0.0
 * @since 10-August-2018
 */

'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
var config = require('../config/config.json');
const ora = require('ora')
var persist = require('../persist/persist-store')

var readlineSync = require('readline-sync');
var auth = require('./auth');

var StellarSdk = require('stellar-sdk');
var request = require('request');

exports.changeTrust = exports.issueAsset = undefined;

exports.changeTrust = changeTrust;
exports.issueAsset = issueAsset;

//TrustLine DB Config
var dbType = config.dbType;
var dbIp = config.dbIp;
var dbPort = config.dbPort;
var dbUser = config.dbUser;
var dbPass = config.dbPass;
var dbName = config.dbName;

var connectionString = "";
var db;

var promise = require('bluebird');

var options = {
    // Initialization Options
    promiseLib: promise
};
var pgp = require('pg-promise')(options);
var mysql = require('mysql');
var connection;

if (dbType === "postgres") {
    connectionString = dbType + "://" + dbUser + ":" + dbPass + "@" + dbIp + ":" + dbPort + "/" + dbName + "?sslmode=disable";
    db = pgp(connectionString);
} else if (dbType === "mysql") {
    connection = mysql.createConnection({
        host: dbIp,
        port: dbPort,
        user: dbUser,
        password: dbPass,
        database: dbName
    });
    connection.connect(function (err) {
        if (!err) {
            // console.log(dbType + " Database is connected ... \n\n");
        } else {
            console.log("Error connecting database ... \n\n");
        }
    });
}

/**
 * @author:Girijashankar Mishra
 * @description: This funcation will create the trust-line between users.
 * @param {networkType,firstName,lastName,accountSeed,asset,accountNo,accountIfsc} args 
 * @param {JSON} res
 */
async function changeTrust(args) {
    const spinner = ora().start();
    try {
        persist.store.getItem('accountId').then(function (data) {
            const network = args.networkType || args.n;

            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
                StellarSdk.Network.useTestNetwork();
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
                StellarSdk.Network.usePublicNetwork();
            }

            if (data) {
                trust(server, args, spinner);
                spinner.stop();
            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                trust(server, args, spinner);
                spinner.stop();
            }
            spinner.stop();
        });
    } catch (err) {
        spinner.stop();

        console.error(err)
    }
}

/**
 * @author:Girijashankar Mishra
 * @description: This function will issue asset to account holders who are willing to accept issuer asset as currency.
 * @param {networkType,issuerSeed,asset,receiver,assetAmount} args 
 * @param {JSON} res
 */
async function issueAsset(args) {
    const spinner = ora().start();
    try {
        persist.store.getItem('accountId').then(function (data) {
            const network = args.networkType || args.n;
            var server;
            if (network === "test") {
                server = new StellarSdk.Server(config.stellarServerTest);
                StellarSdk.Network.useTestNetwork();
            } else if (network === "public") {
                server = new StellarSdk.Server(config.stellarServerPublic);
                StellarSdk.Network.usePublicNetwork();
            }
            if (data) {

                issue(server, args, spin);
                spinner.stop();

            } else {
                console.log('Please login to complete your transaction.');
                var userAccountId = readlineSync.question('Enter your AccountId : ');
                var arg = {};
                arg["n"] = "test";
                arg["a"] = userAccountId;
                var resp = auth.login(arg);
                issue(server, args, spin);
                spinner.stop();
            }
            spinner.stop();
        });
    } catch (err) {
        spinner.stop();

        console.error(err)
    }
}


/**
 * @author:Girijashankar Mishra
 * @description: This funcation will create the trust-line between users.
 * @param {server, args, spinner} args 
 * @param {JSON} res
 */
function trust(server, args, spinner) {

    const firstName = args.firstName || args.f;
    const lastName = args.lastName || args.l;
    const seed = args.accountSeed || args.s;
    const asset = args.asset || args.a;
    const accountNo = args.accountNo || args.c;
    const accountIfsc = args.accountIfsc || args.i;

    const issuer = config.issuerAccount;

    var keyPairs = StellarSdk.Keypair.fromSecret(seed);


    var accountId = keyPairs.publicKey();
    var friendlyId = firstName + '_' + lastName;
    // First, the receiving account must trust the asset
    server.loadAccount(keyPairs.publicKey())
        .then(function (receiver) {

            var transaction = new StellarSdk.TransactionBuilder(receiver)
                // The `changeTrust` operation creates (or alters) a trustline
                // The `limit` parameter below is optional
                .addOperation(StellarSdk.Operation.changeTrust({
                    asset: new StellarSdk.Asset(asset, issuer),
                    // limit: amountIssued
                }))
                .build();

            transaction.sign(keyPairs);
            server.submitTransaction(transaction);
            if (dbType === "postgres") {
                db.none('insert into accounts(id, first_name, last_name, name, domain, friendly_id, bank_account, bank_ifsc)' +
                        'values(${id}, ${first_name}, ${last_name}, ${name}, ${domain}, ${friendly_id}, ${bank_account}, ${bank_ifsc})',
                        body)
                    .then(function () {
                        spinner.stop();
                        console.log('Trustline created successfully.');
                    })
                    .catch(function (err) {
                        spinner.stop();
                        console.error('Error!', error);
                    });
            } else if (dbType === "mysql") {
                var sql = "INSERT INTO internal_accounts (account_id, first_name, last_name, name, domain, friendly_id, account_no, account_ifsc) VALUES " +
                    "('" + accountId + "', '" + firstName + "', '" + lastName + "', '" + firstName + "', '" + config.domainName + "'" +
                    ", '" + friendlyId + "', '" + accountNo + "', '" + accountIfsc + "')";
                connection.query(sql, function (err, result) {
                    if (err) throw err;
                    // console.log("1 record inserted");
                    spinner.stop();
                    console.log('Trustline created successfully.');
                });
            }

        })
        .catch(function (error) {
            console.error('Error!', error);
        });
}

/**
 * @author:Girijashankar Mishra
 * @description: This function will issue asset to account holders who are willing to accept issuer asset as currency.
 * @param {server, args, spinner} args 
 * @param {JSON} res
 */
function issue(server, args, spinner) {
    const seed = args.issuerSeed || args.s;
    const receiver = args.receiver || args.r;
    const assetAmount = args.assetAmount || args.m;
    const asset = args.asset || args.a;

    var issuingKeys = StellarSdk.Keypair.fromSecret(seed);
    server.loadAccount(issuingKeys.publicKey())
        .then(function (issuer) {
            var transaction = new StellarSdk.TransactionBuilder(issuer)
                .addOperation(StellarSdk.Operation.payment({
                    destination: receiver,
                    asset: new StellarSdk.Asset(asset, issuingKeys.publicKey()),
                    amount: assetAmount
                }))
                .build();
            transaction.sign(issuingKeys);
            server.submitTransaction(transaction);
            spinner.stop();
            console.log(JSON.parse(JSON.stringify(transaction)));
        });
}